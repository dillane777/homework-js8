document.documentElement.setAttribute('lang', 'en')

let metautf8 = document.createElement('meta')
metautf8.setAttribute('charset', 'UTF-8')

let title = document.createElement('title')
title.innerHTML = 'Call to Action'

document.head.appendChild(metautf8)
document.head.appendChild(title) 

let style = document.createElement('style')
style.innerHTML = `
    .h1 {
        font-family: 'Arvo, sans-serif';
        font-size: 36px;
        line-height: 48px;
        text-align: center;
        margin-top: 122px;
        margin-bottom: 10px;
    }
    .subtitle {
        text-align: center;
        font-family: 'OpenSans, sans-serif';
        font-size: 14px;
        lineHeight: 26px;
        color: #9FA3A7;
        margin-bottom: 55px;
    }  
    .container {
        display: flex;
        justify-content: center; 
        margin-bottom: 139px;       
    }  
    .block_left {
        display: flex;
        flex-direction: column;
        align-items: center;
        width: 401px;
        border: 1px solid #E8E9ED;
    }
    .block_right {
        display: flex;
        flex-direction: column;
        align-items: center;
        width: 401px;
        background-color: #8F75BE;
    }
    .sup_left{
        font-family: 'Montserrat, sans-serif';
        font-size: 12px;
        line-height: 15px;
        color: #9FA3A7;
        margin-top: 80px;
    }
    .sup_right{
        font-family: 'Montserrat, sans-serif';
        font-size: 12px;
        line-height: 15px;
        color: #FFC80A;
        margin-top: 80px;
    }
    .h2_left {
        font-family: 'Arvo, sans-serif';
        font-size: 36px;
        line-height: 46px;
        width: 210px;
        text-align: center;
        margin-top: 19px;        
    }
    .h2_right {
        font-family: 'Arvo, sans-serif';
        font-size: 36px;
        line-height: 46px;
        width: 210px;
        text-align: center;
        margin-top: 19px;
        color: #ffff;        
    }
    .text_left {
        font-family: 'OpenSans, sans-serif';
        font-size: 12px;
        line-height: 22px;
        text-align: center;
        color: #9FA3A7;
        width: 210px;
        margin-top: 25px;
        margin-bottom: 52px;
    }
    .text_right {
        font-family: 'OpenSans, sans-serif';
        font-size: 12px;
        line-height: 22px;
        text-align: center;
        color: #ffff;
        width: 210px;
        margin-top: 25px;
        margin-bottom: 52px;
    }
    .button_left {
        width: 147px;
        height: 46px;
        border-radius: 150px;
        border: 3px solid #FFC80A;
        background-color: #fff;
        font-family: 'Montserrat, sans-serif';
        font-size: 12px;
        line-height: 15px;
        margin-bottom: 90px;
        cursor: pointer;
    }
    .button_right {
        width: 147px;
        height: 46px;
        border-radius: 150px;
        border: 3px solid #FFC80A;
        background-color: #8F75BE;
        font-family: 'Montserrat, sans-serif';
        font-size: 12px;
        line-height: 15px;
        margin-bottom: 90px;
        cursor: pointer;
    }
`
document.head.appendChild(style)


let h1 = document.createElement('h1')
h1.classList.add('h1')
document.body.appendChild(h1)
h1.innerHTML = 'Choose Your Option'

let subtitle = document.createElement('p')
subtitle.classList.add('subtitle')
document.body.appendChild(subtitle)
subtitle.innerHTML = 'But I must explain to you how all this mistaken idea of denouncing '

let container = document.createElement('container')
container.classList.add('container')
document.body.appendChild(container)

let block_left = document.createElement('div')
block_left.classList.add('block_left')
container.appendChild(block_left)

let block_right = document.createElement('div')
block_right.classList.add('block_right')
container.appendChild(block_right)

let sup_right = document.createElement('h3')
sup_right.innerHTML = 'STUDIO'
sup_right.classList.add('sup_right')
block_right.appendChild(sup_right)

let sup_left = document.createElement('h3')
sup_left.classList.add('sup_left')
sup_left.innerHTML = 'FREELANCER'
block_left.appendChild(sup_left)

let h2_left = document.createElement('h2')
h2_left.innerHTML = 'Initially designed to'
h2_left.classList.add('h2_left')
block_left.appendChild(h2_left)

let h2_right = document.createElement('h2')
h2_right.innerHTML = 'Initially designed to'
h2_right.classList.add('h2_right')
block_right.appendChild(h2_right)

let text_left = document.createElement('p')
text_left.innerHTML = 'But I must explain to you how all this mistaken idea of denouncing '
text_left.classList.add('text_left')
block_left.appendChild(text_left)

let text_right = document.createElement('p')
text_right.innerHTML = 'But I must explain to you how all this mistaken idea of denouncing '
text_right.classList.add('text_right')
block_right.appendChild(text_right)

let button_left = document.createElement('button')
button_left.innerText = 'START HERE'
button_left.classList.add('button_left')
block_left.appendChild(button_left)

let button_right = document.createElement('button')
button_right.innerText = 'START HERE'
button_right.classList.add('button_right')
block_right.appendChild(button_right)






















